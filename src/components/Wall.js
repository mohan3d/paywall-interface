import React, { Component } from 'react';

import PostBox from './PostBox';
import PostList from './PostList';

import { isLoggedIn,  getPosts,  getUpdates,  hasNextPosts,  createPost, init } from '../paywallapi';


class Wall extends Component {
  constructor(props){
    super(props);
    this.state = {
      posts: [],
      updates: [],
      posting: false,
    };

    this.loadMorePosts = this.loadMorePosts.bind(this);
    this.updatePosts = this.updatePosts.bind(this);
    this.sharePost = this.sharePost.bind(this);
  }

  handleError(err) {
    // TODO: handle errors in UI level.
    console.log(err);
  }

  loadMorePosts() {
    getPosts()
    .then(resp => resp.results)
    .then(posts => this.setState((prevState) => ({ posts: prevState.posts.concat(posts) })))
    .catch(err => this.handleError(err));
  }

  fetchUpdates() {
    const posts = this.state.posts;
    if(!posts.length) return;

    getUpdates(this.state.posts[0].created)
    .then(resp => resp.data)
    .then(updates => this.setState({ updates }))
    .catch(err => this.handleError(err));
  }
  
  updatePosts() {
    this.setState((prevState) => ({
      posts: prevState.updates.concat(prevState.posts),
      updates: []
    }));
  }

  sharePost(content) {
    this.setState({posting: true});
    this.updatePosts();

    createPost(content)
    .then(resp => resp.data)
    .then(post => [post])
    .then(posts => {
      this.setState((prevState) => ({ posts: posts.concat(prevState.posts) }));
      this.setState({posting: false});
    })
    .catch(err => {
      this.handleError(err);
      this.setState({posting: false});
    });
  }

  componentDidMount() {
    init();
    this.loadMorePosts();
    this.timer = setInterval(() => this.fetchUpdates(), 2 * 1000);
  }

  componentWillUnmount() {
    this.timer = null;
  }  

  render() {
    return (
      <div className="wall">
        {
          isLoggedIn() &&  
          <PostBox posting={this.state.posting} share={this.sharePost} />
        }
        
        {
          !isLoggedIn() && 
          <section className="description">
            <h1>See what’s happening in the world right now</h1>            
          </section>
        } 

        {
          <PostList 
            posts={this.state.posts}
            unreadPosts={this.state.updates.length}
            updatePosts={this.updatePosts}
            loadMore={this.loadMorePosts} 
            hasPosts={hasNextPosts()}
          />
        }
        
      </div>
    );
  }
}

export default Wall;