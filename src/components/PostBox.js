import React, { Component } from 'react';


class PostBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: '',
    }

    this.handleShare = this.handleShare.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({ content: event.target.value });
  }

  handleShare() {
    if(!this.state.content) return;
    
    this.props.share(this.state.content);
    this.setState({ content: '' });
  }

  render() {
    return (
      <section className="postbox">
        <div className="container">

          <div className="description">
            <h1>Share your thoughts</h1>
          </div>

          <div>
            <div className="postcontent">
              <textarea
                onChange={this.handleChange} 
                value={this.state.content} 
                placeholder="What's happening?">
              </textarea>
            </div>

            <div className="controllers">
              <button
                disabled={this.props.posting}
                onClick={() => this.handleShare()}>
                  Share
              </button>
            </div>            
          </div>
        </div>        
      </section>
    );
  }
}

export default PostBox;