import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Login from './components/Login';
import Logout from './components/Logout';
import Register from './components/Register';
import Wall from './components/Wall';
import { isLoggedIn } from "./paywallapi";

import './App.css';

const AppRouter = () => (
  <Router>
    <div>
      <nav>
        <ul>          
          { !isLoggedIn() && <li><Link to="/login">Login</Link></li> }
          { !isLoggedIn() && <li><Link to="/register">Register</Link></li> }
          { isLoggedIn() &&  <li><Link to="/logout">Logout</Link></li> }
          <li className="home"><Link to="/">Home</Link></li>
        </ul>
      </nav>

      <Route path="/" exact component={Wall} />
      <Route path="/login" component={Login} />
      <Route path="/logout" component={Logout} />
      <Route path="/register" component={Register} />
    </div>
  </Router>
);

export default AppRouter;