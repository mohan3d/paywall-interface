import React, { Component } from 'react';
import Post from './Post';


class PostList extends Component {
  render() {
    return (
      <section className="container">
        {
          this.props.unreadPosts > 0 &&
          <div className="updates">
            <button className="unread" onClick={() => this.props.updatePosts()}>
              You have {this.props.unreadPosts} new posts
            </button>
          </div>
        }
        <div className="posts">
          {this.props.posts.map(post => <Post onClick={this.props.onClick} key={post.id} post={post} />)}
        </div>
        <div className="controllers">
          {
            this.props.hasPosts &&
            <button className="load-more" onClick={() => this.props.loadMore()}>Load more</button>
          }
        </div>
      </section>  
    );
  }
}

export default PostList;