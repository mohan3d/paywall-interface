import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import { register } from '../paywallapi';


class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      username: '',
      password: ''
    }

    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handleUsernameChange = this.handleUsernameChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleRegister = this.handleRegister.bind(this);
  }

  handleEmailChange(event) {
    this.setState({ email: event.target.value });
  }

  handleUsernameChange(event) {
    this.setState({ username: event.target.value });
  }

  handlePasswordChange(event) {
    this.setState({ password: event.target.value });
  }

  handleRegister(event) {
    event.preventDefault();
    if(!(this.state.email && this.state.username && this.state.password)) return;

    register(this.state.email, this.state.username, this.state.password)
    .then(resp => this.props.history.push("/login"))
    .catch(err => console.error(err));
  }

  render() {
    return (
      <div className="form">
        <form>
            <div className="form-input">
            <label>
              Email
              <input onChange={this.handleEmailChange} value={this.state.email} type="email" name="email" />
            </label>
          </div>
          <div className="form-input">
            <label>
              Username
              <input onChange={this.handleUsernameChange} value={this.state.username} type="text" name="username" />
            </label>
          </div>
          <div className="form-input">
            <label>
              Password
              <input onChange={this.handlePasswordChange} value={this.state.password} type="password" name="password" />
            </label>
          </div>
          <button onClick={this.handleRegister}>Register</button>
        </form>
      </div>
    );
  }
}

export default withRouter(Register);