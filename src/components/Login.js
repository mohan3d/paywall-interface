import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import { login } from '../paywallapi';

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: ''
    }

    this.handleUsernameChange = this.handleUsernameChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
  }

  handleUsernameChange(event) {
    this.setState({ username: event.target.value });
  }

  handlePasswordChange(event) {
    this.setState({ password: event.target.value });
  }

  handleLogin(event) {
    event.preventDefault();
    if(!(this.state.username && this.state.password)) return;

    login(this.state.username, this.state.password)
    .then(resp => this.props.history.push("/"))
    .catch(err => console.error(err));
  }

  render() {
    return (
      <div className="form">
        <form>
          <div className="form-input">
            <label>
              Username
              <input onChange={this.handleUsernameChange} value={this.state.username} type="text" name="username" />
            </label>
          </div>
          <div className="form-input">
            <label>
              Password
              <input onChange={this.handlePasswordChange} value={this.state.password} type="password" name="password" />
            </label>
          </div>
          <button onClick={this.handleLogin}>Login</button>
        </form>
      </div>
    );
  }
}

export default withRouter(Login);