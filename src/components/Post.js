import React, { Component } from 'react';


class Post extends Component {
  render() {
    return (
      <div className="post">
        <div className="content">{this.props.post.content}</div>
      </div>
    )
  }
}

export default Post;