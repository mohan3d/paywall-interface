import axios from 'axios';

const WALL_APP_AUTH_TOKEN = 'wallappauthtoken';

const API_VERSION = 'v1';
const API_URL = `http://localhost:8000/api/${API_VERSION}`;
let POSTS_URL;


export {
  login,
  logout,
  register,
  isLoggedIn,
  getPosts,
  getUpdates,
  hasNextPosts,
  createPost,
  init
};

/**
 * Reads authorization token from localStorage.
 *
 * @returns {string}
 */
function getAuthorizationToken() {
  return localStorage.getItem(WALL_APP_AUTH_TOKEN);
}

/**
 * Returns true if there is no saved authorization token in localStorage.
 *
 * @returns {boolean}
 */
function isLoggedIn() {
  return getAuthorizationToken() !== null;
}

/**
 * Creates headers object with optional Authorization header.
 * If no authToken exists Authorization header is removed.
 *
 * @param authToken authorization token.
 * @returns {object}
 */
function getHeaders(authToken) {
  const token = authToken !== null ? authToken : getAuthorizationToken();
  const headers = {};

  if (token != null) {
    headers['Authorization'] = ` Token ${token}`;
  }

  return headers;
}

/**
 * Login with provided credentials,
 * returns promise with authToken from server.
 *
 * If loggedIn successfully authToken is saved to localStorage.
 *
 * @param username credentials username.
 * @param password credentials password.
 * @returns {Promise<string>}
 */
function login(username, password) {
  return axios.post(`${API_URL}/login/`, {
      username,
      password
    })
    .then(resp => {
      const token = resp.data.token;
      localStorage.setItem(WALL_APP_AUTH_TOKEN, token);
      return resp;
    });
}


/**
 * Registers a new user with provided credentials.
 *
 * @param email    credentials email.
 * @param username credentials username.
 * @param password credentials password.
 * @returns {Promise<Response>}
 */
function register(email, username, password) {
    return axios.post(`${API_URL}/register/`, {
        email,
        username,
        password
    });
}

/**
 * Removes AuthToken from localStorage.
 */
function logout() {
  localStorage.removeItem(WALL_APP_AUTH_TOKEN);
}

/**
 * Requests posts list.
 *
 * @returns {Promise<Response>}
 */
function getPosts() {
  if(POSTS_URL === null) return;

  return axios.get(POSTS_URL)
  .then(resp => resp.data)
  .then(resp => {
    POSTS_URL = resp.next;
    return resp;
  });
}

/**
 * Fetches latest updates (posts after a specific datetime).
 * 
 * @param {string} timestamp 
 */
function getUpdates(timestamp) {
  return axios.get(`${API_URL}/posts/${timestamp}`);
}

/**
 * Creates new post with the provided content.
 *
 * @param content text of post to be created.
 * @returns {Promise<Response>}
 */
function createPost(content) {
  return axios.post(`${API_URL}/posts/`, { content }, {
    headers: getHeaders(null)
  });
}

/**
 * Returns true if there are more posts to load, otherwise false.
 * 
 * @returns {boolean}
 */
function hasNextPosts() {
  return POSTS_URL !== null && POSTS_URL !== `${API_URL}/posts/`;
}

/**
 * Initializes POSTS_URL to posts endpoint.
 */
function init() {
  POSTS_URL = `${API_URL}/posts/`;
}