import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import { logout } from '../paywallapi';

class Logout extends Component {
  handleLogout() {
    logout();
    this.props.history.push("/login");
  }

  render() {
    return (
      <div className="form">
        <button onClick={() => this.handleLogout()}>Sure you want to leave?</button>
      </div>
    );
  }
}

export default withRouter(Logout);