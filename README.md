
# Paywall-Interface  
A ReactJS application to consume [Paywall API](https://bitbucket.org/mohan3d/paywall/).

## Installation
```sh
$ cd paywall-interface
$ npm install
```

## Run Locally
**Note:** Don't forget to run django project.

```sh
$ npm start
```

Your app should now be running on [localhost:3000](http://localhost:3000/).


## Testing 
```sh
$ npm run test
```

## Structure
```
src/paywallapi.js: paywall API wrapper in JS, makes it easy to interact with paywall api.

src/App.js: App Router.

src/components: all components required for react app.
```